<?php

//error_reporting(E_ERROR);

//session_start();

//require_once "config.php";
//session_start();
spl_autoload_register("mainLoader");

//****** Автозагрузка классов *******/
function mainLoader($className)
{
    //var_dump($className);
    $arr = explode("\\", $className);
    $path = $className;
    if (count($arr) > 1)    // поддержка namespace
        $path = str_replace('\\', "/", $className);

    require_once $path . ".php";
}

