<?php

/** @var \controllers\BaseController|\controllers\IndexController $this */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?= $this->title; ?></title>

</head>
<body>

<div class="container">

    <div class="starter-template">
        <h1><?= $this->blockTitle; ?></h1>

        <?php $this->renderView("form_upload"); ?>

        <p>
            <span>Поиск файла по имени</span>
            <input id="file_name" type="text">
            <input type="button" id="btn_filter" value="Найти">

            <div id="table_wrapper">
            <?= $this->renderTable(); ?>
            </div>

        </p>
    </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="application/javascript" src="assets/main.js"></script>

</body>


</html>
