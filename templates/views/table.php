<?php

/** @var \src\FileItem[] $fileItems */
$isTableExists = is_array($fileItems) && !empty($fileItems);

if (!isset($sortType))
    $sortType = "";

if ($sortType == "desc")
    $sortTitle = "Я...А";
elseif ($sortType == "asc")
    $sortTitle = "А...Я";
else
    $sortTitle = "сортировать";

?>
<div id="table_wrapper">



    <hr>

    <?php if ($isTableExists): ?>

        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>
                    Имя файла<br>
                    <input type="button" name="f" field="name" value="<?= $sortTitle; ?>" sort_type="<?= $sortType; ?>">
                </th>
                <th>Время загрузки</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($fileItems as $item): ?>
                <tr>
                    <td><?= $item->id; ?></td>
                    <td><?= $item->fileName; ?></td>
                    <td><?= $item->date; ?></td>

                </tr>
            <?php endforeach; ?>



            </tbody>


        </table>
    <?php else: ?>
        Загруженных файлов нет
    <?php endif; ?>

</div>