<?php


namespace src;


class FileEngine
{
    const DIR_SAVE = __DIR__ . "/../storage/";

    private static $_db = null;

    private function __construct()
    {
    }

    public static function getSavedFile($f_id, $jsonMode = true)
    {
        $fileData = self::getDb()->selectFiles($f_id);
        if (!$fileData)
            return json_encode(['response' => "file with ID = $f_id does't exist"], true);

        $xml = simplexml_load_file( self::fullFilePath($fileData['f_name']));
        if ($xml)
            $jsonObj = json_encode($xml, true);
        else
            json_encode(['response' => "request error"], true);

        return $jsonObj;
    }

    private static function fullFilePath($fileName)
    {
        return self::DIR_SAVE . $fileName;
    }


    public static function getSavedFiles($sortMode = "", $fileMask = "")
    {
        $result = [];
        if ($fileMask == "")
            $rows = self::getDb()->selectFiles(0, $sortMode);
        else
            $rows = self::getDb()->selectFilesLike($fileMask);

        if (!is_array($rows) || empty($rows))
            return $result;

        $arr = FileItem::create($rows);
        return $arr;
    }

    /**
     * @param $filePathSrc  full file path of file
     * @param $newFileName  only file name without DIR name
     * @return bool|string
     */
    public static function saveFile($filePathSrc, $newFileName)
    {
        $newFilePath = self::DIR_SAVE . $newFileName;
        if(!copy($filePathSrc, $newFilePath))
        {
            //TODO process
            die("FileSystem Error! Can't copy file $newFilePath");
            //return false;
        }

        $data = self::getDb()->selectRowWithFileName($newFileName);
        if( $data )
            return self::getDb()->update($data['f_id']);
        else
            return self::getDb()->insertFile($newFileName);
    }

    private static function getDb()
    {
        if (!self::$_db)
            self::$_db = new Db();
        return self::$_db;
    }


}