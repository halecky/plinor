<?php

/**
 * Класс для работы с БД
 */

namespace src;

use PDO;


class Db
{
    const CFG_FILE = __DIR__ . "/../config.php";

    /** @var PDO  */
    private $conn = null;

    public function __construct()
    {
        $this->connect();
        $this->checkValidDb();
    }

    private function checkValidDb()
    {
        $sth = $this->conn->query("SHOW TABLES LIKE 'files'");
        $data = $sth->fetch();

        $data or  die("SQL Error! Please create sql dump!");

    }

    public function selectTask($taskID)
    {
        /** @var \PDOStatement $sth */
        $sth = $this->conn->query("SELECT * FROM tasks WHERE id=$taskID");
        $data = $sth->fetch();
        return $data;
    }


    public function selectFiles($id = 0, $sortMode = "", $fileMask = "")
    {
        $query = "SELECT * FROM files";

        if ($id > 0)
            $query .= " WHERE f_id=$id";

        if ($sortMode != "")
            $query .= " ORDER BY f_name $sortMode";

        /** @var \PDOStatement $sth */
        $sth = $this->conn->query($query);
        if ($id > 0)
            $data = $sth->fetch();
        else
            $data = $sth->fetchAll();

        return $data;
    }

    public function selectFilesLike($fileMask)
    {
        $query = "SELECT * FROM files WHERE f_name LIKE :fileMask";

        $sth = $this->conn->prepare($query);

        $data['fileMask'] = "$fileMask%";
        if ($sth->execute($data))
            return $sth->fetchAll();

        return null;
    }

    public function selectRowWithFileName($fileName)
    {
        $query = "SELECT f_id FROM files WHERE f_name=:fileName";
        /** @var \PDOStatement $sth */
        $sth = $this->conn->prepare($query);

        $data['fileName'] = $fileName;
        if ($sth->execute($data))
            return $sth->fetch();

        return null;
    }

    public function insertFile($file)
    {
        $query = "INSERT files(f_name, f_date) VALUES(:file, CURTIME())";
        /** @var \PDOStatement $sth */
        $sth = $this->conn->prepare($query);

        $data['file'] = $file;

        if ($sth->execute($data))
            return $this->conn->lastInsertId();

        return false;
    }

    //update date_time of file
    public function update($f_id)
    {

        $query = "UPDATE files SET f_date = CURTIME() WHERE f_id = $f_id";
        /** @var \PDOStatement $sth */
        $sth = $this->conn->prepare($query);

        if ($sth->execute())
            return $sth->rowCount();

        return false;
    }

    public function connect()
    {
        $config = require_once (__DIR__ . "/../config.php");
        $host = $config['db']['host'];
        $dbname = $config['db']['dbname'];
        $login = $config['db']['user'];
        $password = $config['db']['password'];

        $dsn = "mysql:host=$host;dbname=$dbname;charset=utf8";

        $options = [
            PDO::MYSQL_ATTR_FOUND_ROWS   => TRUE,
            PDO::ATTR_PERSISTENT => true
            , PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            , PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ];

        try {
            $this->conn = new PDO($dsn, $login, $password, $options);
        } catch (PDOException $e) {
            die("DB Error<br>" . $e->getMessage());
        }

        return true;
    }



}