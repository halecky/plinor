<?php


namespace src;


class FileItem
{
    public $id = 0;
    public $fileName = "";
    public $date = "";


    private function __construct()
    {
    }

    public static function create($dataDb)
    {
        if (is_array($dataDb) && !empty($dataDb))
        {
            foreach ($dataDb as $rowDb)
                $result[] = static::createInstance($rowDb);
        } else
            $result = static::createInstance($dataDb);

        return $result;
    }

    private static function createInstance($rowDb)
    {
        $item = new static();
        $item->id = $rowDb['f_id'];
        $item->fileName = $rowDb['f_name'];
        $item->date = $rowDb['f_date'];
        return $item;
    }

}