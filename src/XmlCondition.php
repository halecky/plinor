<?php


namespace src;


class XmlCondition
{
    private $_rootTag = "";
    private $_filterRootAttributes = [];
    private $_filterChilds = [];

    public function __construct($tagName)
    {
        $this->_rootTag = $tagName;
    }

    public function addRootAttrFilter($attributeName, $attributeValue)
    {
        $this->_filterRootAttributes[$attributeName] = $attributeValue;
    }

    public function addChildFilter($childTag, $childFilters = [])
    {
        $this->_filterChilds[$childTag] = $childFilters;
    }

    public function debug()
    {
        var_dump($this->_filterRootAttributes);
    }

    public function getQuery()
    {
        $filterAttr = "";
        foreach ($this->_filterRootAttributes as $attrName => $attrValue)
        {
            $filterAttr .= "[@$attrName='$attrValue']";
        }

        $queryParts[] = "//";
        $queryParts[] = $this->_rootTag;
        $queryParts[] = $filterAttr;
        $queryParts[] = $this->getChildFilterQueryStr();
        $queryParts[] = "/..";

        return implode("", $queryParts);
    }

    private function getChildFilterQueryStr()
    {
        $filterChildsArr = [];
        foreach ($this->_filterChilds as $childTagName => $filter)
        {

            $tagUpper = mb_strtoupper($childTagName);
            $childFilter="*[translate(name(),'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ') = '$tagUpper']";
            if (!empty($filter))
            {
                foreach ($filter as $filterContent)
                    $childFilter .= "[$filterContent]";
            }
            $filterChildsArr[] = $childFilter;
        }

        if (!empty($filterChildsArr))
            $str = "/";
        $str .= implode("/../", $filterChildsArr);

        return $str;
    }

    public function isFileValid($file)
    {
        $xml = simplexml_load_file($file);
        if (!$xml)
            return false;

        $query = $this->getQuery();
        if (empty($query))
            return false;

        $result = $xml->xpath($query);

        return count($result) > 0;
    }

}