<?php


namespace controllers;


use src\FileEngine;
use \src\XmlCondition;

class IndexController extends BaseController
{

    public function renderTable()
    {
        $fileItems = FileEngine::getSavedFiles();
        $viewParams = [
            'fileItems' => $fileItems,
            'sortType' => ""
        ];
        $this->renderView("table", $viewParams);
    }

    public function uploadFile()
    {
        $file = $_FILES['file']['tmp_name'];
        $newFileName = $_FILES['file']['name'];

        $xmlCond = new XmlCondition("Component");
        $xmlCond->addRootAttrFilter("Id", "030-032-000-008");
        $xmlCond->addChildFilter("Limit", [".=''"]);
        $xmlCond->addChildFilter("Value", [".=''"]);
        $xmlCond->addChildFilter("Error", [".='ERROR'"]);

        if($xmlCond->isFileValid($file))
        {
            FileEngine::saveFile($file, $newFileName);
        }

        self::gotoMainPage();
    }


}