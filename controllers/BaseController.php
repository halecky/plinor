<?php

namespace controllers;


class BaseController
{
    const TEMPLATES_PATH = __DIR__ . "/../templates/";
    const VIEWS_PATH = self::TEMPLATES_PATH . "views/";
    const TEMPLATE_DEFAULT = "main";

    protected $_templateName = self::TEMPLATE_DEFAULT;

    public $title = "title";
    public $blockTitle = "";

    public function __construct()
    {
    }

    public function render($templateName, $params = [])
    {
        if (!empty($params))
            extract($params);

        return include_once(self::TEMPLATES_PATH . "$templateName.php");
    }

    public function renderView($viewName, $params = [])
    {
        if (!empty($params))
            extract($params);

        return include_once(self::VIEWS_PATH . "$viewName.php");
    }

    public function renderContent()
    {
    }

    public function show()
    {
        $this->render($this->_templateName);
    }

    public static function gotoMainPage()
    {
        header("Location: /");
        die();
    }
}
