<?php


namespace controllers;

use src\FileEngine;

class ApiController extends BaseController
{
    const ACTION_SORT = "sort";
    const ACTION_FILTER = "filter";
    const ACTION_JSON = "json";


    private static $action="";
    private static $sortType="";
    private static $fileFilter = "";

    public static function processRequest()
    {
        if (isset($_REQUEST['action']))
            self::$action = $_REQUEST['action'];

        switch (self::$action)
        {
            case self::ACTION_SORT:
                self::$sortType = $_REQUEST['sortType'];
                if ( empty(self::$sortType) )
                    self::$sortType = "desc";
                break;

            case self::ACTION_FILTER:
                self::$fileFilter = $_REQUEST['file_name'];
                break;
        }

        if (isset($_REQUEST['files']))
            self::processFiles();


    }

    private static function processFiles()
    {
        if (self::$action == self::ACTION_SORT )
            self::outputTableSort();
        elseif (self::$action == self::ACTION_FILTER)
            self::outputTableFilter();
        elseif (self::$action == self::ACTION_JSON)
            self::outputJson();
        else
            echo json_encode(['response' => "error request"]);
    }

    private static function outputTableSort()
    {
        $fileItems = FileEngine::getSavedFiles(self::$sortType, self::$fileFilter);
        $ctrl = new self;

        $viewParams = [
            'fileItems' => $fileItems,
            'sortType' => self::$sortType == "asc" ? "desc":"asc"
        ];

        $ctrl->renderView("table", $viewParams);
    }

    private static function outputTableFilter()
    {
        $fileItems = FileEngine::getSavedFiles(self::$sortType, self::$fileFilter);
        $ctrl = new self;

        $viewParams = [
            'fileItems' => $fileItems
        ];

        $ctrl->renderView("table", $viewParams);
    }

    private static function outputJson()
    {
        $f_id = intval($_REQUEST['files']);
        if ($f_id > 0)
            echo FileEngine::getSavedFile($f_id);
        else {
            $fileItems = FileEngine::getSavedFiles(self::$sortType, self::$fileFilter);
            $json['files'] = $fileItems;
            $r = json_encode($json, true);
            echo $r;
        }

    }

}