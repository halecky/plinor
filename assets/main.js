$(function() {

    var url = "api.php";
    $("body").on('click', "[field][sort_type]", function () {

        var sortType = $(this).attr('sort_type');
        reloadTable({ action: "sort", files: 0, sortType: sortType });
    });

    $("body").on('click', "#btn_filter", function () {
        reloadTable({ action: "filter", files: 0, file_name: $("#file_name").val() });
    });


    function reloadTable(postData)
    {
        var url = "api.php";
        $("#table_wrapper").load(url, postData);
    }

});